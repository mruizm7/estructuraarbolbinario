import java.util.*;
public class IntBST
{
    protected IntBSTNode root;
    
    //CONSTRUCTOR SIN PARAMETROS
    public IntBST(){ // Crea una estrutura de datos tipo arbol binario vacia
       root = null;
    }
    
    protected void visit(IntBSTNode p){
        System.out.print(p.key + " - ");
    }
    
    //METODO BUSQUEDA CON COMPLEJIDAD LOGARITMICA 
    public IntBSTNode search(IntBSTNode p, int el){
         while(p != null)
             if(el == p.key)
                return p;
             else
                if( el < p.key)
                   p = p.left;
                else
                   p = p.right;
         return null;
    }
    
    public void insert(int el){
       IntBSTNode p =root, prev = null;
       while( p != null ){
          prev = p;
          if( p.key < el)
              p = p.right;
          else
              p = p.left;
        }
        if ( root == null)
           root = new IntBSTNode(el);
        else
           if ( prev.key < el )
               prev.right = new IntBSTNode(el);
           else
               prev.left = new IntBSTNode(el);
    }        
    
    public void breadthFirst(){
       IntBSTNode p = root;
       Queue queue = new LinkedList();
       if ( p != null ){
          queue.add(p); // Agregar a la fila
          while( !queue.isEmpty() ){
               p = (IntBSTNode) queue.poll();
               visit(p);
               if( p.left != null )
                  queue.add(p.left);
               if( p.right != null )
                  queue.add(p.right);
            }
          
        }
    }
    
    protected void inorder(IntBSTNode p){
        if (  p != null){
           inorder(p.left);
           visit(p);
           inorder(p.right);
        }
    }
    
    protected void postorder(IntBSTNode p){
        if (  p != null){
           postorder(p.left);
           postorder(p.right);
           visit(p);
        }
    }
    
    protected void preorder(IntBSTNode p){
        if (  p != null){
           visit(p);
           preorder(p.left);
           preorder(p.right);
        }
    }
    
    
    }
    
    
    
    
    
    
    
    
    
    

