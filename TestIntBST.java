public class TestIntBST
{
  public static void main(String args[]){
          IntBST arbol = new IntBST();
          arbol.insert(15);
          arbol.insert(4);
          arbol.insert(1);
          arbol.insert(20);
          arbol.insert(16);
          arbol.insert(25);
          System.out.println("Recorrido en amplitud");
          arbol.breadthFirst();
          System.out.println();
          System.out.println("Recorrido en profundidad - inorder");
          arbol.inorder(arbol.root);
          System.out.println();
          System.out.println("Recorrido en profundidad -preorder");
          arbol.preorder(arbol.root);
          System.out.println();
          System.out.println("Recorrido en profundidad - postorder");
          arbol.postorder(arbol.root);
    }
}
